export function ok (result) {
  if (result instanceof Error) {
    return err(result)
  }

  return {
    error: false,
    error_msg: null,
    error_code: null,
    result,
  }
}

export function err (result) {
  if (!(result instanceof Error)) {
    return ok(result)
  }

  return {
    error: true,
    error_msg: result?.msg ??
      result.message ??
      result?.originalError?.message ??
      result?.originalError?.msg ??
      'There is an error but no message. (should not happen!)',
    error_code: result?.code ?? result?.originalError?.code ?? 99999,
  }
}
