
import dayjs from './dayjs/index.mjs'
import { START_OF_THE_GAME_PROVINCE_TIME_EPOCH, START_OF_THE_GAME_REAL_TIME_EPOCH } from '../date/constants.mjs'
/*
Ref: https://docs.province.gg/technical-documentation/time
Provincial time is complex. It runs at 5x the speed of normal time due to the conflict that Province was created ()
All time is done in Unix Epoch Time and must be expressed in milliseconds.
The start date of the game in real time was Feb 5, 2022 14:30:00.
The start date of the game in Provincial Time was  Jan 6, 2031 14:30:00.
*/

export function nowInMilliseconds () {
  return dayjs().valueOf()
}

export function provinceTimeToReal (provinceTime) {
  return addUnitToDate({
    date: dayjs(START_OF_THE_GAME_REAL_TIME_EPOCH),
    milliseconds: Math.round((dayjs(provinceTime) - dayjs(START_OF_THE_GAME_PROVINCE_TIME_EPOCH)) / 5),
  }).valueOf()
}

export function realTimeToProvinceTime (realTime) {
  return addUnitToDate({
    date: dayjs(START_OF_THE_GAME_PROVINCE_TIME_EPOCH),
    milliseconds: (dayjs(realTime) - dayjs(START_OF_THE_GAME_REAL_TIME_EPOCH)) * 5,
  }).valueOf()
}

/*
supported Units {
  date: string
  day?: number
  week?: number
  month?: number
  quarter?: number
  year?: number
  hour?: number
  minute?: number
  second?: number
  millisecond?: number
}
*/
export function addUnitToDate (args) {
  const { date, ...units } = args
  let result = dayjs(date)

  Object.entries(units).forEach(([unit, amount]) => {
    result = result.add(amount, unit)
  })

  return result.valueOf()
}

export default dayjs
