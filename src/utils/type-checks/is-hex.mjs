export function isHex (hexValue) {
  const re = /^(0x)?[0-9A-Fa-f]+$/
  return re.test(hexValue)
}
