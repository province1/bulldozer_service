import lodash from 'lodash'
const { isEmpty, isNil } = lodash

export function isNumericOrNumericString (stringOrNumber) {
  if (typeof stringOrNumber === 'number') return !isNaN(stringOrNumber)
  return !isNil(stringOrNumber) && !isEmpty(stringOrNumber) && !isNaN(Number(stringOrNumber))
}
