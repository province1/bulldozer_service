import abind from 'abind'
import StatusCodes from 'http-status-codes'

import { err, ok } from '../utils/error/index.mjs'

export class EquipmentSaleController {
  constructor (saleService) {
    this.saleService = saleService
    abind(this)
  }

  async startSale (req, res) {
    const { serial } = req.body

    try {
      const response = await this.saleService.placeEscrowLock({ serial })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }

  async finalizeSale (req, res) {
    const { serial, sale_id: saleId } = req.body
    const { force_as: forceAs } = req.query

    try {
      const response = await this.saleService.removeEscrowLock({ serial, saleId, forceAs })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }
}
