import lodash from 'lodash'
import abind from 'abind'
import camelcase from 'camelcase'
import StatusCodes from 'http-status-codes'

import { err, ok } from '../utils/error/index.mjs'

const { isNil } = lodash

export class DetailsController {
  constructor (detailsService) {
    this.detailsService = detailsService
    abind(this)
  }

  async queryDetails (req, res) {
    const { serial, detail } = req.params

    try {
      const response = await this.detailsService.getDetails({
        serial,
        detail: !isNil(detail) ? camelcase(detail) : detail,
      })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }
}
