import abind from 'abind'
import StatusCodes from 'http-status-codes'
import httpContext from 'express-http-context'

import { err, ok } from '../utils/error/index.mjs'

export class OperationsController {
  constructor (operationService) {
    abind(this)
    this.operationsService = operationService
  }

  async startDeployment (req, res) {
    const { equipmentType, serial } = req.params
    const { senderAddress } = httpContext.get('authData')

    try {
      const response = await this.operationsService.startDeployment({
        senderAddress,
        equipmentType,
        serial,
      })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }

  async doStatusChecksWithUpdate (req, res) {
    const { serial, address, plot_int: plotInt } = req.body
    try {
      const response = await this.operationsService.execStatusChecks({ serial, address, plotInt })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }
}
