import abind from 'abind'
import StatusCodes from 'http-status-codes'

import { err, ok } from '../utils/error/index.mjs'

export class LeasingController {
  constructor (leasingService) {
    this.leasingService = leasingService
    abind(this)
  }

  async startLeasing (req, res) {
    const { senderAddress, lessee, serial, days } = req.body
  

    try {
      const response = await this.leasingService.leaseEquipment({ senderAddress, lessee, serial, days })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }
}
