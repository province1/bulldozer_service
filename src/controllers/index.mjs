import { EquipmentSaleController } from './sale.mjs'
import { DetailsController } from './details.mjs'
import { OperationsController } from './operations.mjs'
import { LeasingController } from './leasing.mjs'
import { detailsService, leasingService, operationsService, saleService } from '../services/index.mjs'

export const equipmentSaleController = new EquipmentSaleController(saleService)
export const detailsController = new DetailsController(detailsService)
export const operationsController = new OperationsController(operationsService)
export const leasingController = new LeasingController(leasingService)
