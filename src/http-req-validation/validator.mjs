import { checkSchema } from 'express-validator'

import { RequestValidationError } from '../exceptions/request-validation.mjs'

function stringifyErrors (errors) {
  return errors.map(({ msg, param, location }) => `${msg}: ${param} ${location}`).join(' ; ')
}

export function buildValidationMiddleware (validationSchema) {
  return async (req, res, next) => {
    try {
      const checkContexts = await checkSchema(validationSchema).run(req)
      const errors = checkContexts
        .filter(ctx => ctx.errors instanceof Array && ctx.errors.length > 0)
        .map(ctx => ctx.errors)
        .flat()

      if (errors.length > 0) {
        return next(new RequestValidationError(stringifyErrors(errors)))
      }
      next()
    } catch (error) {
      next(error)
    }
  }
}
