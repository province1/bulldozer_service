export default {
  serial: {
    in: ['body'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
}
