export const getDetailsSchema = {
  serial: {
    in: ['params'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
  detail: {
    in: ['params'],
    optional: true,
    notEmpty: true,
    isAlphanumeric: true,
    toLowerCase: true,
    isIn: {
      options: [['owner']],
    },
    errorMessage: 'invalid name for detail',
  },
}
