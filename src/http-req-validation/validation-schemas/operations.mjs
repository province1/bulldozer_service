export const deployEquipmentSchema = {
  serial: {
    in: ['params'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
  equipmentType: {
    in: ['params'],
    notEmpty: true,
    isOptional: false,
    isIn: {
      options: [['bulldozer']],
    },
  },
}

export const doStatusChecksSchema = {
  serial: {
    in: ['body'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
  plot_int: {
    in: ['body'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
  address: {
    in: ['body'],
    notEmpty: true,
    isHexadecimal: true,
    isLength: {
      options: {
        max: 50,
        min: 40,
      },
    },
    toLowerCase: true,
  },
}
