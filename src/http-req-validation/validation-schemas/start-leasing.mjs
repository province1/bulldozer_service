import lodash from 'lodash'

import { RequestValidationError } from '../../exceptions/request-validation.mjs'
import { isHex } from '../../utils/type-checks/is-hex.mjs'
import { isNumericOrNumericString } from '../../utils/type-checks/is-numeric-or-numeric-string.mjs'

const { isNil } = lodash

export default {

  days: {
    in: ['body'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
  senderAddress: {
    in: ['body'],
    notEmpty: true,
    isHexadecimal: true,
    isLength: {
      options: {
        max: 42,
        min: 42,
      },
    },
    toLowerCase: true,
  },
  lessee: {
    in: ['body'],
    notEmpty: true,
    isHexadecimal: true,
    isLength: {
      options: {
        max: 42,
        min: 42,
      },
    },
    
    toLowerCase: true,
  },
  
  serial: {
    in: ['body'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
  

}
