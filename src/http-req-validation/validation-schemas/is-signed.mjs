import lodash from 'lodash'

import { RequestValidationError } from '../../exceptions/request-validation.mjs'
import { isHex } from '../../utils/type-checks/is-hex.mjs'
import { isNumericOrNumericString } from '../../utils/type-checks/is-numeric-or-numeric-string.mjs'

const { isNil } = lodash

export const isSignedSchema = {
  msg: {
    in: ['body'],
    notEmpty: true,
    custom: {
      options: (value) => {
        let payload = {}
        try {
          payload = JSON.parse(value)
        } catch {
          throw new RequestValidationError('Message is not a valid serialized json object')
        }

        if (isNil(payload?.sender_address)) {
          throw new RequestValidationError('Expected sender_address attribute is not present within message')
        }

        if (!isHex(payload.sender_address)) {
          throw new RequestValidationError('Expected sender_address is invalid')
        }

        if (
          isNil(payload?.timestamp) ||
          !isNumericOrNumericString(payload?.timestamp)
        ) {
          throw new RequestValidationError('Expected timestamp is not present or invalid')
        }
        return true
      },
    },
  },
  signature: {
    in: ['body'],
    notEmpty: true,
  },
}
