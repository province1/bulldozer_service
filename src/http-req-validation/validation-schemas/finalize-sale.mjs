export default {
  force_as: {
    in: ['query'],
    optional: true,
    isIn: {
      options: [['aborted', 'cancelled']],
    },
  },
  serial: {
    in: ['body'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
  sale_id: {
    in: ['body'],
    notEmpty: true,
    isUUID: {
      version: 4,
    },
  },
}
