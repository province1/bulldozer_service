import { LandApiClient } from './land-api-client/index.mjs'
import { MaterialsApiClient } from './materials-api-client/index.mjs'

export const landApiClient = new LandApiClient()
export const materialsApiClient = new MaterialsApiClient()
