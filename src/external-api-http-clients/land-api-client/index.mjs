import abind from 'abind'
import lodash from 'lodash'

import BaseHttpClient from '../base-http-client/index.mjs'
import {
  LAND_SERVICE_URL,
  LAND_SERVICE_BASIC_AUTH_USER,
  LAND_SERVICE_BASIC_AUTH_PASSWORD,
} from '../../config/index.mjs'
import { LandAPIRequestError } from '../../exceptions/api-request.mjs'

const { isEmpty } = lodash

export class LandApiClient extends BaseHttpClient {
  constructor () {
    super({ baseURL: LAND_SERVICE_URL })
    this.addAxiosRequestInterceptor((axiosConfig) => {
      if (!isEmpty(axiosConfig.auth)) {
        return axiosConfig
      }

      return ({
        ...axiosConfig,
        auth: {
          username: LAND_SERVICE_BASIC_AUTH_USER,
          password: LAND_SERVICE_BASIC_AUTH_PASSWORD,
        },
      })
    })
    abind(this)
  }

  async clearLand ({ msg, signature }) {
    try {
      const response = await this.post('/api/land/operation/clear', { msg, signature })

      return response
    } catch (error) {
      throw new LandAPIRequestError(
        'land api error, unable to start clearing operation: ' +
        `${error.msg} ${error?.originalError?.data?.error_msg ?? error?.originalError?.data}`,
        error,
      )
    }
  }

  async stopClearing ({ plotInt, owner }) {
    try {
      const response = await this.delete(`/api/land/operation/clear/${plotInt}/${owner}`)

      return response
    } catch (error) {
      throw new LandAPIRequestError(
        'Land api error, unable to stop clearing operation:' +
        `${error.msg} ${error?.originalError?.data?.error_msg ?? error?.originalError?.data}`,
        error,
      )
    }
  }
}
