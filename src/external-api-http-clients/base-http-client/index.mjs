import abind from 'abind'
import axios from 'axios'

import { APIRequestError } from '../../exceptions/api-request.mjs'

export default class BaseHttpClient {
  constructor ({ baseURL }) {
    this._client = axios.create({ baseURL })
    abind(this)
  }

  addAxiosRequestInterceptor (onBeforeSend, onRequestError = undefined) {
    return this._client.interceptors.request.use(onBeforeSend, onRequestError)
  }

  removeAxiosRequestInterceptor (interceptor) {
    this._client.interceptors.request.eject(interceptor)
  }

  async sendRequest (config) {
    try {
      const { method, url, data, headers, params } = config
      const response = await this._client(
        url ?? '',
        { method, data, headers, params },
      )

      return response.data
    } catch (err) {
      if (err.response) {
        throw new APIRequestError(err.message, {
          message: err.message,
          data: err.response.data,
          status: err.response.status,
        })
      } else if (err.request) {
        throw new APIRequestError(err.message)
      } else {
        throw err
      }
    }
  }

  get (url, config = {}) {
    return this.sendRequest(this.getConfig(url, config))
  }

  getConfig (url, config = {}) {
    return { method: 'GET', url, ...config }
  }

  post (url, data, config) {
    return this.sendRequest(this.postConfig(url, data, config))
  }

  postConfig (url, data, config = {}) {
    return { method: 'POST', url, data, ...config }
  }

  put (url, data, config) {
    return this.sendRequest(this.putConfig(url, data, config))
  }

  putConfig (url, data, config = {}) {
    return { method: 'PUT', url, data, ...config }
  }

  patch (url, data, config) {
    return this.sendRequest(this.patchConfig(url, data, config))
  }

  patchConfig (url, data, config = {}) {
    return { method: 'PATCH', url, data, ...config }
  }

  delete (url, data, config) {
    return this.sendRequest(this.deleteConfig(url, data, config))
  }

  deleteConfig (url, data, config = {}) {
    return { method: 'DELETE', url, data, ...config }
  }
}
