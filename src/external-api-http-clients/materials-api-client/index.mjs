import abind from 'abind'
import lodash from 'lodash'

import BaseHttpClient from '../base-http-client/index.mjs'
import {
  MATERIALS_SERVICE_URL,
  MATERIALS_SERVICE_BASIC_AUTH_USER,
  MATERIALS_SERVICE_BASIC_AUTH_PASSWORD,
} from '../../config/index.mjs'
import { MaterialsAPIRequestError } from '../../exceptions/api-request.mjs'

const { isEmpty } = lodash

export class MaterialsApiClient extends BaseHttpClient {
  constructor () {
    super({ baseURL: MATERIALS_SERVICE_URL })
    abind(this)

    this.addAxiosRequestInterceptor((axiosConfig) => {
      if (!isEmpty(axiosConfig.auth)) {
        return axiosConfig
      }

      return ({
        ...axiosConfig,
        auth: {
          username: MATERIALS_SERVICE_BASIC_AUTH_USER,
          password: MATERIALS_SERVICE_BASIC_AUTH_PASSWORD,
        },
      })
    })
  }

  async balance ({ owner, materialType }) {
    try {
      const response = await this.get('/balance', { params: { owner, materialType } })

      return response
    } catch (error) {
      throw new MaterialsAPIRequestError(
        `Unable to get the balance: ${error.msg} ${error?.originalError?.data?.error_msg ?? ''}`,
        error,
      )
    }
  }

  async destroy ({ owner, materialType, amount }) {
    try {
      const response = await this.post('/destroy', { address: owner, type: materialType, amount })

      return response
    } catch (error) {
      throw new MaterialsAPIRequestError(
        'Materials api, unable to destroy the materials: ' +
        `${error.msg} ${error?.originalError?.data?.error_msg ?? error?.originalError?.data}`,
        error,
      )
    }
  }

  async create ({ owner, materialType, amount }) {
    try {
      const response = await this.post('/create', { owner, type: materialType, amount })

      return response
    } catch (error) {
      throw new MaterialsAPIRequestError(
        'Materials api, unable to create the materials: ' +
        `${error.msg} ${error?.originalError?.data?.error_msg ?? error?.originalError?.data}`,
        error,
      )
    }
  }
}
