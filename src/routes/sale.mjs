import { equipmentSaleController } from '../controllers/index.mjs'
import startSaleSchema from '../http-req-validation/validation-schemas/start-sale.mjs'
import finalizeSaleSchema from '../http-req-validation/validation-schemas/finalize-sale.mjs'
import { buildValidationMiddleware } from '../http-req-validation/validator.mjs'

export default function (router) {
  router.post(
    '/sale/start',
    buildValidationMiddleware(startSaleSchema),
    equipmentSaleController.startSale,
  )
  router.post(
    '/sale/end',
    buildValidationMiddleware(finalizeSaleSchema),
    equipmentSaleController.finalizeSale,
  )
}
