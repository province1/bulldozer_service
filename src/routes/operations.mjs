import { operationsController } from '../controllers/index.mjs'
import { deployEquipmentSchema, doStatusChecksSchema } from '../http-req-validation/validation-schemas/operations.mjs'
import { isSignedSchema } from '../http-req-validation/validation-schemas/is-signed.mjs'
import { buildValidationMiddleware } from '../http-req-validation/validator.mjs'
import signatureValidator from '../middlewares/signature-validator.mjs'



export default function (router) {
  router.post(
    '/equipment-ops/deploy/:equipmentType/:serial',
    buildValidationMiddleware(isSignedSchema),
    signatureValidator,
    buildValidationMiddleware(deployEquipmentSchema),
    operationsController.startDeployment,
  )

  router.post(
    '/equipment-ops/status-checks-trigger',
    buildValidationMiddleware(doStatusChecksSchema),
    operationsController.doStatusChecksWithUpdate,
  )
}
