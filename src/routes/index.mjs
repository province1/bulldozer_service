import { Router } from 'express'

import mountSaleRoutes from './sale.mjs'
import mountDetailsRoutes from './details.mjs'
import mountOperationsRoutes from './operations.mjs'
import mountLeasingRoutes from './leasing.mjs'

const router = Router()

mountSaleRoutes(router)
mountDetailsRoutes(router)
mountOperationsRoutes(router)
mountLeasingRoutes(router)

export default router
