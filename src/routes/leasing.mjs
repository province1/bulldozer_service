import { leasingController } from '../controllers/index.mjs'
import startLeasingSchema from '../http-req-validation/validation-schemas/start-leasing.mjs'
import { buildValidationMiddleware } from '../http-req-validation/validator.mjs'
import { basicAuthAuthenticator, buildAuthenticationMiddleware } from '../middlewares/authentication.mjs';

export default function (router) {
  router.post(
    '/leasing',
    buildAuthenticationMiddleware(basicAuthAuthenticator),
    buildValidationMiddleware(startLeasingSchema),
    leasingController.startLeasing,
  )
}
