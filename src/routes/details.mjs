import { detailsController } from '../controllers/index.mjs'
import { getDetailsSchema } from '../http-req-validation/validation-schemas/details.mjs'
import { buildValidationMiddleware } from '../http-req-validation/validator.mjs'

export default function (router) {
  router.get(
    '/equipment-details/:serial/:detail?',
    buildValidationMiddleware(getDetailsSchema),
    detailsController.queryDetails,
  )
}
