import { isNumericOrNumericString } from '../utils/type-checks/is-numeric-or-numeric-string.mjs'

export const MICRO_SERVICE_NAME = process.env.NAME || 'bulldozer-service'
export const ENVIRONMENT = process.env.NODE_ENV || 'local'
export const IS_PRODUCTION = ENVIRONMENT === 'production' || ENVIRONMENT === 'prod'
export const IS_LOCAL_ENV = ENVIRONMENT === 'local'
export const PORT = process.env.PORT || 3000
export const EVENTS_DATABASE_SERVICE_URL = process.env.DB_SERVICE_URL || 'http://localhost:3001'
export const EVENTS_DATABASE_SERVICE_USERNAME = process.env.DB_SERVICE_ID ?? 'anonymous'
export const EVENTS_DATABASE_SERVICE_PASSWORD = process.env.DB_SERVICE_KEY ?? ''

export const _5_MIN_IN_MILLISECONDS = 5 * 60 * 1000
export const SIGNATURE_EXPIRATION_THRESHOLD_IN_MILLISECONDS =
  isNumericOrNumericString(process.SIGNATURE_EXPIRATION_THRESHOLD_IN_MILLISECONDS)
    ? Number(process.SIGNATURE_EXPIRATION_THRESHOLD_IN_MILLISECONDS)
    : _5_MIN_IN_MILLISECONDS

export const LAND_SERVICE_URL = process.env.LAND_SERVICE_URL ?? 'http://localhost:3000/api'
export const LAND_SERVICE_BASIC_AUTH_USER = process.env.LAND_SERVICE_BASIC_AUTH_USER
export const LAND_SERVICE_BASIC_AUTH_PASSWORD = process.env.LAND_SERVICE_BASIC_AUTH_PASSWORD

export const MATERIALS_SERVICE_URL = process.env.MATERIALS_SERVICE_URL || 'http://localhost:3002/api'
export const MATERIALS_SERVICE_BASIC_AUTH_USER = process.env.MATERIALS_SERVICE_BASIC_AUTH_USER
export const MATERIALS_SERVICE_BASIC_AUTH_PASSWORD = process.env.MATERIALS_SERVICE_BASIC_AUTH_PASSWORD

export const DAYS_IN_YEAR_FLOAT = 365.25
export const MILLISECONDS_IN_A_DAY = 86400000 // 24 hr * 3600 sec/hr * 1000 millis/hr

export const SALES_SERVICE_API_KEY = process.env.SALES_SERVICE_API_KEY
export const ALLOWED_API_KEYS = [SALES_SERVICE_API_KEY]


export const BASIC_AUTH_USER = process.env.BASIC_AUTH_USER
export const BASIC_AUTH_PASSWORD = process.env.BASIC_AUTH_PASSWORD
