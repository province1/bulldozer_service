import lodash from 'lodash'
import snakecaseKeys from 'snakecase-keys'

import { EventsDatabaseUnexpectedStateError } from '../exceptions/events-database.mjs'

const { isEmpty, isNil } = lodash

export class DetailsService {
  constructor (externalEquipmentDatabaseService) {
    this.equipmentEventsDatabaseService = externalEquipmentDatabaseService
  }

  async getDetails ({ serial, detail }) {
    const currentState = await this.equipmentEventsDatabaseService.getCurrentState({ serial })

    let result = { ...currentState }

    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for equipment')
    }

    if (!isNil(detail)) {
      result = { [detail]: currentState[detail] }
    }

    return snakecaseKeys(result)
  }
}
