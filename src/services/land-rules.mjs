import lodash from 'lodash'

import { DAYS_IN_YEAR_FLOAT, MILLISECONDS_IN_A_DAY } from '../config/index.mjs'
import { nowInMilliseconds, realTimeToProvinceTime } from '../utils/date/index.mjs'

// game rules for land

// real speed for fast growing tress is 200 centimeters per year
// 2000 millimeters / 365.25 days
// average tree heigh is 20 meters = 20000 millimeters
const TREE_GROWTH_SPEED_PER_DAY = 2000 / DAYS_IN_YEAR_FLOAT
const TREE_GROWTH_SPEED_PER_MILLISECOND = TREE_GROWTH_SPEED_PER_DAY / MILLISECONDS_IN_A_DAY

// trees will never will grow more than this value
const TREE_AVERAGE_HEIGH_IN_MILLIMETERS = 20000

const { isNil, min } = lodash

export class LandRules {
  static rule (type, ruleName, ...args) {
    const ruleId = [type, ruleName].filter(r => !isNil(r)).join('.')
    const rules = {
      'tree.current-growth': (clearedAtInProvinceTime) => {
        const now = realTimeToProvinceTime(nowInMilliseconds())
        const growingIntervalInMilliseconds = now - clearedAtInProvinceTime
        // never allow trees grow more than TREE_AVERAGE_HEIGH_IN_MILLIMETERS
        const currentTreeHeigh = min([
          TREE_GROWTH_SPEED_PER_MILLISECOND * growingIntervalInMilliseconds,
          TREE_AVERAGE_HEIGH_IN_MILLIMETERS,
        ])
        const heighLimit = LandRules.rule('tree', 'height')
        const currentTreeHeighRatio = currentTreeHeigh / heighLimit

        return { currentTreeHeigh, currentTreeHeighRatio }
      },
      'tree.height': () => {
        return TREE_AVERAGE_HEIGH_IN_MILLIMETERS
      },
      'land.max-timber-harvest': () => {
        return 90000 // 90 tns
      },
      'land.current-timber-harvest': (clearedAtInProvinceTime) => {
        const { currentTreeHeighRatio } = LandRules.rule('tree', 'current-growth', clearedAtInProvinceTime)
        const timberHarvest = LandRules.rule('land', 'max-timber-harvest')
        return Math.round(timberHarvest * currentTreeHeighRatio)
      },
    }

    if (isNil(rules[ruleId]) || !(rules[ruleId] instanceof Function)) {
      throw new Error(`invalid or undefined rule ${ruleId}`)
    }

    return rules[ruleId](...args)
  }
}
