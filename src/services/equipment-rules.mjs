import lodash from 'lodash'
import { addUnitToDate, nowInMilliseconds, realTimeToProvinceTime } from '../utils/date/index.mjs'

const { isNil } = lodash

export class EquipmentRules {
  static apply (equipmentType, ruleName, ...args) {
    const ruleId = [equipmentType, ruleName].join('.')
    const rules = {
      'bulldozer.endOfClearing': (...args) => {
        const [startDateTime] = args
        const effectiveStart = startDateTime ?? nowInMilliseconds()
        return addUnitToDate({
          date: realTimeToProvinceTime(effectiveStart),
          days: 90,
        })
      },
    }

    if (isNil(rules[ruleId]) || !(rules[ruleId] instanceof Function)) {
      throw new Error('invalid or undefined rule')
    }

    return rules[ruleId](...args)
  }
}
