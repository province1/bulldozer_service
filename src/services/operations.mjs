import abind from 'abind'
import httpContext from 'express-http-context'

import { LandAPIRequestError } from '../exceptions/api-request.mjs'
import { nowInMilliseconds, realTimeToProvinceTime } from '../utils/date/index.mjs'
import { EquipmentRules } from './equipment-rules.mjs'
import { LandRules } from './land-rules.mjs'

import { has_custody, idle_check, leasee_access, expired_lease, escrow_check } from './service_helpers/checks.mjs'

export class OperationsService {
  constructor (externalEquipmentDatabaseService, landApiClient, materialsApiClient) {
    this.equipmentEventsDatabaseService = externalEquipmentDatabaseService
    this.landApiClient = landApiClient
    this.materialsApiClient = materialsApiClient
    abind(this)
  }


  deploy_checks(equipment_state, senderAddress){
    has_custody(equipment_state, senderAddress)
    idle_check(equipment_state)
    leasee_access(equipment_state, senderAddress)
    expired_lease(equipment_state, senderAddress)
    escrow_check(equipment_state)
  }


  async isBulldozer ({ serial }) {
    const state = await this.getCurrentState({ serial })
    const re = /^.*bulldozer.*$/i

    return re.test(state.item)
  }
    
    
    
  async startDeployment ({ senderAddress, equipmentType, serial }) {
    const currentState = await this.equipmentEventsDatabaseService.getCurrentState({ serial })
    const iAmLessee = await this.equipmentEventsDatabaseService.amILessee({ serial, address: senderAddress })

    this.deploy_checks(currentState, senderAddress );

    try {
      const busyUntil = EquipmentRules.apply(equipmentType, 'endOfClearing')
      const effectiveBusyUntil = iAmLessee ? currentState.leaseEndsAt : busyUntil
      const { msg, signature } = httpContext.get('authData')

      await this.equipmentEventsDatabaseService.putNewState({
        ...currentState,
        busyUntil: effectiveBusyUntil,
        timestamp: nowInMilliseconds(),
        status: 'deployed',
      })
      await this.landApiClient.clearLand({ msg, signature })
    } catch (error) {
      console.log("land error", error)
      if (error instanceof LandAPIRequestError) {
        // rollback on land service failure for clearing
        await this.equipmentEventsDatabaseService.putNewState(currentState)
      }

      throw error
    }
  }

  async execStatusChecks ({ serial, address, plotInt }) {
    const currentEquipmentState = await this.equipmentEventsDatabaseService.getCurrentState({ serial })
    const iAmOwner = await this.equipmentEventsDatabaseService.amIOwner({ serial, address })
    const iAmLessee = await this.equipmentEventsDatabaseService.amILessee({ serial, address })
    const isBulldozer = await this.equipmentEventsDatabaseService.isBulldozer({ serial })

    let newEquipmentState = { ...currentEquipmentState }
    let stateUpdated = false

    if (currentEquipmentState.status === 'deployed' && (iAmLessee || iAmOwner) && isBulldozer) {
      const effectiveBusyUntil = (
        currentEquipmentState.busyUntil ??
        EquipmentRules.apply('bulldozer', 'endOfClearing', currentEquipmentState.timestamp)
      )
      const nowInProvince = realTimeToProvinceTime(nowInMilliseconds())

      if (nowInProvince >= effectiveBusyUntil) {
        newEquipmentState = { ...newEquipmentState, status: 'idle', busyUntil: undefined }
        await this.landApiClient.stopClearing({ plotInt, owner: address })

        const amount = LandRules.rule('land', 'current-timber-harvest', effectiveBusyUntil)
        await this.materialsApiClient.create({ owner: address, materialType: 'timber', amount })

        stateUpdated = true
      }
    }

    if (stateUpdated) {
      await this.equipmentEventsDatabaseService.putNewState(newEquipmentState)
    }

    return newEquipmentState
  }
}
