import { DetailsService } from './details.mjs'
import { LeasingService } from './leasing.mjs'
import { OperationsService } from './operations.mjs'
import { SaleService } from './sale.mjs'

import { equipmentDatabaseService, saleDatabaseService } from '../external-service-integrations/index.mjs'
import { landApiClient, materialsApiClient } from '../external-api-http-clients/index.mjs'

export const saleService = new SaleService(equipmentDatabaseService, saleDatabaseService)
export const operationsService = new OperationsService(equipmentDatabaseService, landApiClient, materialsApiClient)
export const detailsService = new DetailsService(equipmentDatabaseService)
export const leasingService = new LeasingService(equipmentDatabaseService)
