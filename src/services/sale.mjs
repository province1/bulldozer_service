import lodash from 'lodash'

import { EventsDatabaseUnexpectedStateError } from '../exceptions/events-database.mjs'
import { OperationalError } from '../exceptions/operational.mjs'
import { nowInMilliseconds } from '../utils/date/index.mjs'
import { leasingService } from './index.mjs'

import { active_lease_check } from './service_helpers/checks.mjs'

const { isEmpty, isNil } = lodash

export class SaleService {
  constructor (
    externalEquipmentDatabaseService,
    externalSaleDatabaseService,
  ) {
    this.equipmentEventsDatabaseService = externalEquipmentDatabaseService
    this.saleEventsDatabaseService = externalSaleDatabaseService
  }

  async placeEscrowLock ({ serial }) {
    const currentState = await this.equipmentEventsDatabaseService.getCurrentState({ serial })

    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Equipment item not found')
    }

    if (currentState?.escrow === true) {
      throw new OperationalError('Equipment already palaced for sale')
    }

    if (active_lease_check(currentState)) {
      throw new OperationalError('Cannot sell equipment with an active lease')
    }

    const result = await this.equipmentEventsDatabaseService.putNewState({
      ...currentState,
      escrow: true,
      timestamp: nowInMilliseconds(),
    })

    return result
  }

  async removeEscrowLock ({ serial, saleId, forceAs }) {
    const shouldCancel = forceAs === 'cancelled'
    const shouldAbort = forceAs === 'aborted'
    const isNormalEnding = isNil(forceAs)

    const currentState = await this.equipmentEventsDatabaseService.getCurrentState({ serial })
    const currentSaleState = await this.saleEventsDatabaseService.getCurrentState({ saleId })

    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for equipment')
    }

    if (isEmpty(currentSaleState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for sale')
    }

    const equipmentSeller = currentSaleState?.seller
    const equipmentBuyer = currentSaleState?.buyer

    if (`${equipmentSeller}`.toLowerCase() !== `${currentState?.owner}`.toLowerCase()) {
      throw new EventsDatabaseUnexpectedStateError('Only owner is allowed to finalize a sale')
    }

    if (isNil(currentSaleState?.state)) {
      throw new EventsDatabaseUnexpectedStateError('Sale status is undefined aborting')
    }

    if (shouldCancel && currentSaleState?.state !== 'cancelled') {
      throw new EventsDatabaseUnexpectedStateError('Sale cannot be terminated, cancelled state is expected')
    }

    if ((shouldAbort || isNormalEnding) && currentSaleState?.state !== 'ended') {
      throw new EventsDatabaseUnexpectedStateError('Sale cannot be terminated, ended state is expected')
    }

    if (serial !== currentSaleState?.serial) {
      throw new EventsDatabaseUnexpectedStateError('Sale equipment serial does not match the requested')
    }

    const newOwner = (shouldCancel || shouldAbort) ? currentState.owner : equipmentBuyer

    const result = await this.equipmentEventsDatabaseService.putNewState({
      ...currentState,
      escrow: false,
      owner: newOwner,
      timestamp: nowInMilliseconds(),
    })

    return result
  }
}
