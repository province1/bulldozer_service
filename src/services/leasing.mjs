import { OperationalError } from '../exceptions/operational.mjs'
import { addUnitToDate, nowInMilliseconds, realTimeToProvinceTime } from '../utils/date/index.mjs'

import { idle_check, owner_check, existance_check, active_lease_check } from './service_helpers/checks.mjs'

export class LeasingService {
  constructor (externalEquipmentDatabaseService) {
    this.equipmentEventsDatabaseService = externalEquipmentDatabaseService
  }

  
  
  lease_checks(equipment_state, senderAddress){
    
    existance_check(equipment_state)
    idle_check(equipment_state)
    owner_check(equipment_state, senderAddress)
    if (active_lease_check(equipment_state)) {
      throw new OperationalError('Equipment is being leased')
    }
    
  }

  async leaseEquipment ({ senderAddress, lessee, serial, days }) {
    const currentEquipmentState = await this.equipmentEventsDatabaseService.getCurrentState({ serial })

    this.lease_checks(currentEquipmentState, senderAddress)

    const leaseEndsAt = addUnitToDate({
      date: realTimeToProvinceTime(nowInMilliseconds()),
      day: days,
    })

    const result = await this.equipmentEventsDatabaseService.putNewState({
      ...currentEquipmentState,
      timestamp: nowInMilliseconds(),
      lessee,
      leaseEndsAt,
      escrow: false
    })

    return result
  }
}
