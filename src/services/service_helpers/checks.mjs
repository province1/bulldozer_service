

import { OperationalError } from '../../exceptions/operational.mjs'

import { addUnitToDate, nowInMilliseconds, realTimeToProvinceTime } from '../../utils/date/index.mjs'

import { EventsDatabaseUnexpectedStateError } from '../../exceptions/events-database.mjs'


import lodash from 'lodash'
const { isEmpty, isNil } = lodash


export const amIOwner = ( equipment_state, address )=> {
    return !isNil(equipment_state.owner) && `${equipment_state.owner}`.toLocaleLowerCase() === `${address}`.toLocaleLowerCase()
};

const amILessee = (equipment_state, address ) => { // true if address is a lessor
    return !isNil(equipment_state?.lessee) && `${equipment_state.lessee}`.toLocaleLowerCase() === `${address}`.toLocaleLowerCase()
}


const has_active_lease = (equipment_state) =>{
    if (isNil(equipment_state.lessee) || isNil(equipment_state.leaseEndsAt)) { return false }
    const nowInProvince = realTimeToProvinceTime(nowInMilliseconds())
    return nowInProvince < equipment_state.leaseEndsAt
}

export const idle_check = (equipment_state) => {
    if(equipment_state?.status !== 'idle') throw new OperationalError('Equipment is being used, unable to deploy')
}



export const owner_check = (equipment_state, address) => {
    const owner_check_result = amIOwner( equipment_state, address);
    if(!owner_check_result) throw new OperationalError('Failed Ownership Test')
}



export const has_custody = (equipment_state, address) => {
    
    const owner_check = amIOwner( equipment_state, address);
    const lessee_check = amILessee( equipment_state, address)
    
    if (!owner_check && !lessee_check) {
      throw new OperationalError('You need to be the owner or lessee of deployed equipment')
    }
}

export const leasee_access = (equipment_state, address) => {
    const active_lease = has_active_lease(equipment_state);
    const lessee_check = amILessee(equipment_state, address)
    if (active_lease && !lessee_check) {
      throw new OperationalError('Equipment is being leased, unable to deploy')
    }
}

export const expired_lease = (equipment_state, address) => {
    const active_lease = has_active_lease(equipment_state);
    const lessee_check = amILessee(equipment_state, address)
    if (!active_lease && lessee_check) {
      throw new OperationalError('Your lease has ended')
    }
}

export const existance_check = (currentState) => {
    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for equipment')
    }
}
 
export const active_lease_check = (equipment_state) => {
    
    if (isNil(equipment_state.lessee) || isNil(equipment_state.leaseEndsAt)) { return false }
    const nowInProvince = realTimeToProvinceTime(nowInMilliseconds())
    return nowInProvince < equipment_state.leaseEndsAt

    
}


export const escrow_check = ( equipment_state ) => {
    if(!isNil(equipment_state.escrow) && equipment_state.escrow) throw new OperationalError('Item is under escrow.')
}
