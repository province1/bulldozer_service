import { BaseError } from './base-error.mjs'

export class APIRequestError extends BaseError { }

export class LandAPIRequestError extends APIRequestError { }

export class MaterialsAPIRequestError extends APIRequestError { }
