import StatusCodes from 'http-status-codes'

export class BaseError extends Error {
  constructor (msgOrError, originalError, code) {
    super(msgOrError ?? originalError)
    const msg = msgOrError instanceof Error ? msgOrError.message : msgOrError

    this._code = code ?? originalError?.code ?? StatusCodes.INTERNAL_SERVER_ERROR
    this._msg = msg ?? originalError?.msg ?? originalError?.message
    this._originalError = originalError
  }

  get msg () {
    return this._msg
  }

  get code () {
    return this._code
  }

  get originalError () {
    return this._originalError
  }
}
