import StatusCodes from 'http-status-codes'

import { BaseError } from './base-error.mjs'

export class AuthenticationError extends BaseError {
  constructor (msg, originalError, code) {
    super(msg, originalError, code ?? StatusCodes.UNAUTHORIZED)
    this._msg = msg ?? `Authentication Error ${this?.msg ? ' - ' + this.msg : ''}`
  }
}

export class InvalidSignatureError extends BaseError {
  constructor (msg, originalError, code) {
    super(msg, originalError, code ?? StatusCodes.UNAUTHORIZED)
    this._msg = msg ?? 'Invalid signature'
  }
}

export class EthersVerificationError extends BaseError {
  constructor (msg, originalError, code) {
    super(msg, originalError, code ?? StatusCodes.INTERNAL_SERVER_ERROR)
    this._msg = msg ?? 'Ethers verification failed'
  }
}

export class ExpiredSignatureError extends BaseError {
  constructor (msg, originalError, code) {
    super(msg, originalError, code ?? StatusCodes.GONE)
    this._msg = msg ?? 'Expired signature'
  }
}
