
import pkg from 'lodash';
const { isEmpty, isNil } = pkg;


import {
  ALLOWED_API_KEYS,
  BASIC_AUTH_USER,
  BASIC_AUTH_PASSWORD,
} from '../config/index.mjs'
import { AuthenticationError } from '../exceptions/auth.mjs'

export function applicationAuthenticator (req) {
  return ALLOWED_API_KEYS.filter(key => !isNil(key)).includes(req?.headers['x-api-key'])
}

export function basicAuthAuthenticator (req) {
  const basicAuthBase64 = (req?.headers.authorization ?? '').split(' ')[1] || ''
  if (isEmpty(basicAuthBase64)) {
    throw new Error('Invalid authentication header')
  }

  if (isEmpty(BASIC_AUTH_USER) || isNil(BASIC_AUTH_USER) || isEmpty(BASIC_AUTH_PASSWORD) || isNil(BASIC_AUTH_PASSWORD)) {
    throw new Error('Basic authentication config is missing')
  }

  const [username, password] = Buffer.from(basicAuthBase64, 'base64').toString().split(':')

  if (username !== BASIC_AUTH_USER || password !== BASIC_AUTH_PASSWORD) {
    throw new Error('Invalid user or password provided')
  }

  return true
}

export function buildAuthenticationMiddleware (authenticator) {
  return function (req, res, next) {
    try {
      if (authenticator(req) === false) {
        throw new Error('Forbidden')
      }
      return next()
    } catch (err) {
      next(new AuthenticationError(err?.message, err))
    }
  }
}

