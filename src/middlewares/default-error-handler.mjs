
import HttpStatus from 'http-status-codes'

import { err } from '../utils/error/index.mjs'
import { RequestValidationError } from '../exceptions/request-validation.mjs'

export default (error, req, res, next) => {
  console.log(error)
  if (res.headersSent) {
    return next(error)
  }

  res.status(HttpStatus.INTERNAL_SERVER_ERROR)

  if (error instanceof RequestValidationError) {
    res.status(error.code)
  }

  res.send(err(error))
}
