import abind from 'abind'
import lodash from 'lodash'
import httpContext from 'express-http-context'

import { EventsDatabaseError } from '../exceptions/events-database.mjs'
import { EventsDatabaseService } from './events-database.mjs'

const { isEmpty, isNil } = lodash

export class EquipmentDatabaseService extends EventsDatabaseService {
  constructor (...args) {
    super(...args)
    abind(this)
    this.fieldsList = [
      'busy_until',
      'description',
      'escrow',
      'img_url',
      'item',
      'lessee',
      'lease_ends_at',
      'mode',
      'new_location',
      'object',
      'owner',
      'sale_status',
      'serial',
      'status',
      'timestamp',
    ]
  }

  _getContextKey ({ serial, equipmentId } = {}) {
    return `${serial}.${equipmentId}`
  }

  async getCurrentState ({ serial, equipmentId } = {}, invalidate = false) {
    const state = httpContext.get(this._getContextKey({ serial, equipmentId }))
    if (!isEmpty(state) && !isNil(state) && !invalidate) {
      return state
    }

    const serialQuery = isNil(serial) ? {} : { serial: Number(serial) }
    const equipmentQuery = isNil(equipmentId) ? {} : { _id: equipmentId }
    const extraQuerySearch = !isEmpty(serialQuery) || !isEmpty(equipmentQuery)
      ? { query: { ...equipmentQuery, ...serialQuery } }
      : { }

    const payload = {
      collection: 'equipment',
      ...extraQuerySearch,
    }
    const operation = 'current_state'
    try {
      const response = await this.sendOperation(payload, operation)

      const currentState = response instanceof Array ? this._convertToAppState(response[0]) : undefined
      httpContext.set(this._getContextKey(currentState), currentState)

      return currentState
    } catch (error) {
      throw new EventsDatabaseError('Unable to get current state', error)
    }
  }

  async putNewState (appState = {}) {
    const stateForAdapter = this._convertToAdapterState(appState)
    if (isEmpty(stateForAdapter)) { return }

    const payload = {
      collection: 'equipment',
      entry_doc: stateForAdapter,
    }

    const operation = 'state_change'

    try {
      const response = await this.sendOperation(payload, operation)

      httpContext.set(this._getContextKey(appState), undefined)
      return response?.result ?? response
    } catch (error) {
      throw new EventsDatabaseError('Unable to update state', error)
    }
  }

  async isEquipmentIdle ({ serial }) {
    const state = await this.getCurrentState({ serial })

    return state?.status === 'idle'
  }

  async amIOwner ({ serial, address }) { // true if address is the owner
    const state = await this.getCurrentState({ serial })

    return !isNil(state.owner) && `${state.owner}`.toLocaleLowerCase() === `${address}`.toLocaleLowerCase()
  }

  async amILessee ({ serial, address }) { // true if address is a lessor
    const state = await this.getCurrentState({ serial })

    return !isNil(state?.lessee) && `${state.lessee}`.toLocaleLowerCase() === `${address}`.toLocaleLowerCase()
  }

  async isBulldozer ({ serial }) {
    const state = await this.getCurrentState({ serial })
    const re = /^.*bulldozer.*$/i

    return re.test(state.item)
  }
}
