import abind from 'abind'
import lodash from 'lodash'

import { EventsDatabaseError } from '../exceptions/events-database.mjs'
import { EventsDatabaseService } from './events-database.mjs'

const { isEmpty, isNil } = lodash

export class SalesDatabaseService extends EventsDatabaseService {
  constructor (...args) {
    super(...args)
    abind(this)
    this.fieldsList = [
      'buyer',
      'end',
      'item_type',
      'plot_int',
      'price',
      'pricing_model',
      'sale_id',
      'sale_type',
      'seller',
      'start',
      'state',
      'timestamp',
      'serial'
    ]
  }

  async getCurrentState ({ plotInt, saleId } = {}) {
    const saleIdQuery = isNil(saleId) ? { } : { sale_id: saleId }
    const landQuery = isNil(plotInt) ? { } : { plot_int: Number(plotInt) }
    const extraQuerySearch = !isEmpty(saleIdQuery) || !isEmpty(landQuery)
      ? { query: { ...landQuery, ...saleIdQuery } }
      : { }

    const payload = {
      collection: 'sales',
      ...extraQuerySearch,
    }
    const operation = 'current_state'

    try {
      const response = await this.sendOperation(payload, operation)

      return response instanceof Array ? this._convertToAppState(response[0]) : undefined
    } catch (error) {
      throw new EventsDatabaseError('Unable to get current state for a sale', error)
    }
  }

  async putNewState (appState = {}) {
    const stateForAdapter = this._convertToAdapterState(appState)
    if (isEmpty(stateForAdapter)) { return }

    const payload = {
      collection: 'sales',
      entry_doc: stateForAdapter,
    }
    const operation = 'state_change'

    try {
      const response = await this.sendOperation(payload, operation)

      return response?.result ?? response
    } catch (error) {
      throw new EventsDatabaseError('Unable to update sale state', error)
    }
  }
}
