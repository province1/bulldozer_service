import { database_service as databaseService } from 'data-service-adapter'

import { EquipmentDatabaseService } from './equipment-database.mjs'
import { SalesDatabaseService } from './sales-events-database.mjs'
import {
  EVENTS_DATABASE_SERVICE_PASSWORD,
  EVENTS_DATABASE_SERVICE_URL,
  EVENTS_DATABASE_SERVICE_USERNAME,
} from '../config/index.mjs'

const dbConnector = databaseService(
  EVENTS_DATABASE_SERVICE_URL,
  EVENTS_DATABASE_SERVICE_USERNAME,
  EVENTS_DATABASE_SERVICE_PASSWORD,
)

export const equipmentDatabaseService = new EquipmentDatabaseService(dbConnector)
export const saleDatabaseService = new SalesDatabaseService(dbConnector)
