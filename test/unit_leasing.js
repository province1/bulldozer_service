

import * as chai from 'chai'

import { leasingService } from '../src/services/index.mjs';

import { nowInMilliseconds, realTimeToProvinceTime } from '../src/utils/date/index.mjs'

chai.should()
let expect = chai.expect


var test_data = {
  description: '...',
  imgUrl: 'https://eep.province.gg/images/bulldozer.png',
  item: 'Bulldozer (G1)',
  newLocation: 1,
  owner: '0x888888',
  serial: 1,
  status: 'idle',
  timestamp: 1657028285259
}

const err_msgs= [
    'Empty sate found for equipment',
    'Failed Ownership Test',
    'Equipment is being used, unable to deploy',
    'Equipment is being leased'

    ]


describe('Higher Level Error checks for leasing', function() {
    
    // IMPORTANT: verify what goes into the existance check when there is no matching equipment found
    it('Should throw an error where no record for the equipment exists', function(done) {
        expect(leasingService.lease_checks.bind(leasingService, [], '0x888888')).to.throw(err_msgs[0])
        expect(leasingService.lease_checks.bind(leasingService, null, '0x888888')).to.throw(err_msgs[0])
        done()
    })
    
    
    it('Should throw an error when the provided owner does not match the listed owner', function(done) {
        expect(leasingService.lease_checks.bind(leasingService, test_data, '0x123')).to.throw(err_msgs[1])
        done()
    })
    
    
    it('Should throw an error if the bulldozer is not idle', function(done) {
        var data_mod_1 = {... test_data}
        data_mod_1.status = "deployed"
        expect(leasingService.lease_checks.bind(leasingService, data_mod_1, test_data.owner)).to.throw(err_msgs[2])
        done()
    })
    
    it('Should throw an error if the owner tries to deploy a bulldozer with an active lease', function(done) {
        var data_mod_3 = {... test_data}
        data_mod_3.lessee = "0x123"
        data_mod_3.leaseEndsAt = realTimeToProvinceTime(nowInMilliseconds()) + 1000000
        expect(leasingService.lease_checks.bind(leasingService, data_mod_3, data_mod_3.owner)).to.throw(err_msgs[3])
        done()
    })
    
   
    

})
