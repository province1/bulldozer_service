

import * as chai from 'chai'

import { operationsService } from '../src/services/index.mjs';

import { addUnitToDate, nowInMilliseconds, realTimeToProvinceTime } from '../src/utils/date/index.mjs'

chai.should()
let expect = chai.expect


var test_data = {
  description: '...',
  imgUrl: 'https://eep.province.gg/images/bulldozer.png',
  item: 'Bulldozer (G1)',
  newLocation: 1,
  owner: '0x888888',
  serial: 1,
  status: 'idle',
  timestamp: 1657028285259
}

const err_msgs= [
    'You need to be the owner or lessee of deployed equipment',
    'Equipment is being used, unable to deploy',
    'Your lease has ended',
    'Equipment is being leased, unable to deploy',
    '',
    '',
    'Item is under escrow'
    ]


describe('Higher Level Error checks for deployments', function() {
    
    it('Should throw an error where the sender is not an owner or lesse', function(done) {
        expect(operationsService.deploy_checks.bind(operationsService, test_data, "0x123")).to.throw(err_msgs[0])
        done()
    })
    
    it('Should throw an error if the bulldozer is not idle', function(done) {
        var data_mod_1 = {... test_data}
        data_mod_1.status = "deployed"
        expect(operationsService.deploy_checks.bind(operationsService, data_mod_1, test_data.owner)).to.throw(err_msgs[1])
        done()
    })
    
    it('Should throw an error if the lease is expired.', function(done) {
        var data_mod_2 = {... test_data}
        data_mod_2.lessee = "0x123"
        data_mod_2.leaseEndsAt = Date.now() - 10000
        expect(operationsService.deploy_checks.bind(operationsService, data_mod_2, "0x123")).to.throw(err_msgs[2])
        done()
    })
    
    it('Should throw an error if the owner tries to deploy a bulldozer with an active lease', function(done) {
        var data_mod_3 = {... test_data}
        data_mod_3.lessee = "0x123"
        data_mod_3.leaseEndsAt = realTimeToProvinceTime(nowInMilliseconds()) + 1000000
        expect(operationsService.deploy_checks.bind(operationsService, data_mod_3, data_mod_3.owner)).to.throw(err_msgs[3])
        done()
    })
    
    it('Should throw an error if the owner tries to deploy a bulldozer with an active lease', function(done) {
        var data_mod_3 = {... test_data}
        data_mod_3.lessee = "0x123"
        data_mod_3.leaseEndsAt = realTimeToProvinceTime(nowInMilliseconds()) + 1000000
        expect(operationsService.deploy_checks.bind(operationsService, data_mod_3, data_mod_3.owner)).to.throw(err_msgs[3])
        done()
    })
    
    
    it('Should not throw an error for a lessee initiated clearing op with a valid lease', function(done) {
        var data_mod_4 = {... test_data}
        data_mod_4.lessee = "0x123"
        data_mod_4.leaseEndsAt = realTimeToProvinceTime(nowInMilliseconds()) + 1000000
        expect(operationsService.deploy_checks.bind(operationsService, data_mod_4, data_mod_4.lessee)).to.not.throw()
        done()
    })
    
    
    it('Should not throw an error for a owner initiated clearing op withouts a valid lease', function(done) {
        var data_mod_5 = {... test_data}
        expect(operationsService.deploy_checks.bind(operationsService, test_data, data_mod_5.owner)).to.not.throw()
        done()
    })
    
    it('Should block a clearing op for equipment with an escrow flag', function(done) {
        var data_mod_6 = {... test_data}
        data_mod_6.escrow = true
        expect(operationsService.deploy_checks.bind(operationsService, data_mod_6, data_mod_6.owner)).to.throw(err_msgs[6])
        done()
    })

    it('Should not throw an error for a lessee initiated clearing op with a valid lease and a negated escrow flag', function(done) {
        var data_mod_7 = {... test_data}
        data_mod_7.lessee = "0x123"
        data_mod_7.leaseEndsAt = realTimeToProvinceTime(nowInMilliseconds()) + 1000000
        expect(operationsService.deploy_checks.bind(operationsService, data_mod_7, data_mod_7.lessee)).to.not.throw()
        done()
    })
    
    it('Should not throw an error for a owner initiated clearing op withouts a valid lease and a negated escrow flag', function(done) {
        var data_mod_8 = {... test_data}
        data_mod_8.escrow = false
        expect(operationsService.deploy_checks.bind(operationsService, test_data, data_mod_8.owner)).to.not.throw()
        done()
    })
    
    
    

})
