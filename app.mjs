import httpContext from 'express-http-context'
import 'dotenv/config'
import cors from 'cors'
import helmet from 'helmet'
import express from 'express'

import { getPctComplete, updateStatus } from './lib/land_clearing.mjs'
import apiRoutes from './src/routes/index.mjs'
import defaultErrorHandler from './src/middlewares/default-error-handler.mjs'

const app = express()
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())

const helmetConfig = { crossOriginResourcePolicy: { policy: 'same-site' } }

app.use(helmet(helmetConfig))
app.use(httpContext.middleware)
app.use('/api', apiRoutes)

const port = process.env.PORT

// Need to ensure that the serial number is included. Otherwise, there is no filter applied
app.post('/clearing_status', async function (req, res) {
  const serial = req.body.serial
  const progress = await getPctComplete(serial)

  res.json(progress)
})

// Need to ensure that the serial number is included. Otherwise, there is no filter applied
app.post('/clearing_run', async function (req, res) {
  // var serial = req.body.serial;
  const status = await updateStatus()

  res.json(status)
})

app.use(defaultErrorHandler)

app.listen(port, () => {
  console.log(`${process.env.NAME} listening at http://localhost:${port}`)
})
