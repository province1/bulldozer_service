
/*
import * as chai from 'chai'

// theres no way around import/export
import { __tests__ } from '../lib/property_register.mjs'
chai.should()
const { TransferEscrowController } = __tests__

const testData = [
  {
    _id: '627525df3c82d8cf363d7cd0',
    start: 1651190400000,
    end: 1651844585000,
    item_type: 'land',
    plot_int: 11,
    sale_type: 'sale',
    seller: '0x5a474871edf7daff7d3910373b55e4d32fa2bfa7',
    pricing_model: 'auction',
    state: 'ended',
    timestamp: 1651844585000,
    sale_id: 'cbd7ddfe-5dac-4ee1-84b3-a04c347b4e6b',
    buyer: '0xb3aa84ee4b01a5be1c67855979934353f2f084df',
  },
  {
    _id: '62725caada185021cded8c9a',
    start: 1651622400000,
    end: 1651705268479,
    item_type: 'land',
    plot_int: 31,
    sale_type: 'sale',
    seller: '0xa7db69778e7dbd420ed13902b7592ed5e10aea39',
    pricing_model: 'fixed-price',
    price: 450000,
    state: 'ended',
    timestamp: 1651705268479,
    sale_id: '9023c315-7041-45d4-800e-ad54eebe4114',
    buyer: '0x4b8562ab83b4584fafc58686201e351ba51c1a97',
  },
  {
    _id: '626ff12bda185021cded8c99',
    start: 1651449600000,
    end: 1651568626710,
    item_type: 'land',
    plot_int: 28,
    sale_type: 'sale',
    seller: '0xa3872bb17bed808915c5594814d127864e310b1b',
    pricing_model: 'fixed-price',
    price: 240000,
    state: 'ended',
    timestamp: 1651568626710,
    sale_id: '0fc66d7e-b1d8-442d-825a-24a487c8e30a',
    buyer: '0x8e375ee38bba256782eb08091d8b16ce7797d028',
  },
  {
    _id: '626fa7d4da185021cded8c98',
    start: 1651449600000,
    end: 1651492924980,
    item_type: 'land',
    plot_int: 28,
    sale_type: 'sale',
    seller: '0x22d0752ec2f3352d2671a4b955a2fa955699a215',
    pricing_model: 'fixed-price',
    price: 180000,
    state: 'ended',
    timestamp: 1651492924980,
    sale_id: '70fa0aa6-f91a-46fa-b797-9aa564179351',
    buyer: '0xa3872bb17bed808915c5594814d127864e310b1b',
  },
  {
    _id: '627537a64d660e112a18e7db',
    start: 1651873803000,
    end: 1652486399000,
    item_type: 'land',
    plot_int: 50,
    sale_type: 'sale',
    seller: '0x5a5f40952638504a380737acb4672cd4ea615bb1',
    pricing_model: 'auction',
    state: 'created',
    timestamp: 1651849126662,
    sale_id: 'd8950178-f017-4940-807f-c37a2a903c94',
  },
]

describe('Calculate the percentage completion of land clearing in progress', async function () {
  const test1 = { it_msg: 'Should throw an error if the sale_id is not present in the data', error_result: true, sale_id: 1234 }
  const test2 = { it_msg: 'Should return false for clearing operations that just started', error_result: false, sale_id: 'd8950178-f017-4940-807f-c37a2a903c94' }
  // const test3 = {status: "deployed", timestamp: test_date_50_pct, it_msg: "Should return false for clearing operations that are 50% complete", process_complete: false }
  // const test4 = {status: "deployed", timestamp: test_date_done, it_msg: "Should return true for clearing operations that are complete and return true for the update operations", process_complete: true }
  // const test5 = {status: "deployed", timestamp: test_date_random_err, it_msg: "Should return false for a clearing operation with an erroneous date starting in the future.", process_complete: false }

  const salesService = () => {
    return testData
  }

  const testArray = [test1, test2/* , test3, test4, test5 ]

  await Promise.all(testArray.map(async x => {
    it(x.it_msg, async () => {
      const test = await TransferEscrowController(1, x.sale_id, salesService)

      test.error.should.equal(x.error_result)

      
            if(test.process_complete == true){
                test.update_handler_result.should.equal(true)
                test.land_handler_result.should.equal(true)
                test.credit_handler_result.should.equal(true)

            }
            
    })
  }))
})
*/