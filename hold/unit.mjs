/*

import * as chai from 'chai';
var should = chai.should();


//theres no way around import/export
import { __tests__ } from '../lib/land_clearing.mjs';
const { completion_check, calc_pct_complete } = __tests__;




// Test Date Calculations

var test_date_start_now = Date.now()
var test_date_50_pct = test_date_start_now - (1000 * 60 *60 * 24 * (365.25 / 12) * 3 * 0.5 / 5 )
var test_date_done = test_date_start_now -8640000000
var test_date_random_err = test_date_start_now + 8640000000


describe('Calculate the percentage completion of land clearing in progress', function() {
    
    it('Should output 50% for a time = 45 days ago in provincial time', function(done) {
        
        // Miliseconds -> Seconds -> Minutes -> Hours -> -> days * # of days in a month * 3 months /2 (50% of total time required) -> provincial time required
        var test_date = Date.now() - (1000 * 60 *60 * 24 * (365.25 / 12) * 3 * 0.5 / 5 )
        calc_pct_complete(test_date).should.equal(50)
        done()
        
    })
    
    it('Should output 0% for a clearing operation starting now', function(done){
        
        calc_pct_complete(Date.now()).should.equal(0)
        done()
        
        
    })
    
    
     it('Should output 100% for a clearing operation that started well over 90 provincial days ago', function(done){
        
        var test_date = Date.now()-8640000000
        calc_pct_complete(test_date).should.equal(100)
        done()
        
        
    })
    
    it('Should output 0% for a clearing operation that has an erroneous date starting in the future.', function(done){
        
        var test_date = Date.now() + 8640000000
        calc_pct_complete(test_date).should.equal(0)
        done()
        
        
    })
    
})


describe('Test each clearing operation for completion, and if complete, make the updates to the bulldozer, land, and material services.', async function() {
    
    var test_1 = {status: "idle", it_msg: "Should return false for bulldozers with status idle", process_complete: false}
    var test_2 = {status: "deployed", timestamp: test_date_start_now, it_msg: "Should return false for clearing operations that just started", process_complete: false}
    var test_3 = {status: "deployed", timestamp: test_date_50_pct, it_msg: "Should return false for clearing operations that are 50% complete", process_complete: false }
    var test_4 = {status: "deployed", timestamp: test_date_done, it_msg: "Should return true for clearing operations that are complete and return true for the update operations", process_complete: true }
    var test_5 = {status: "deployed", timestamp: test_date_random_err, it_msg: "Should return false for a clearing operation with an erroneous date starting in the future.", process_complete: false }
    
    
    const update_handler = () => {
        return true
    }
    
    var test_array = [test_1, test_2, test_3, test_4, test_5];
    
    
    await Promise.all(test_array.map(async x => {
        
        it(x.it_msg, async () => {
        
            var test = await completion_check( x, update_handler, update_handler, update_handler)
            test.process_complete.should.equal(x.process_complete)
            
            if(test.process_complete == true){
                test.update_handler_result.should.equal(true)
                test.land_handler_result.should.equal(true)
                test.credit_handler_result.should.equal(true)
            }
            
        })
        
    }))
    

    
})
*/