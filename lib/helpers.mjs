// const { ethers } = require("ethers");
import Moralis from 'moralis/node'

const ethers = Moralis.web3Library

export function verifySignature (msg, hash) {
  const address = ethers.utils.verifyMessage(msg, hash)

  return ethers.utils.getAddress(address)
}
