
import axios from 'axios'

axios.defaults.baseURL = process.env.SALES_API_URL

// async function escrowValidation (serial, sale_id, sales_service) {
//   try {
//     // Get all the current sales
//     const sales = await sales_service()

//     // Filter for a sale matching the sale_id. Handle cases where sales_service is an empty array.
//     // ******* IMPORTANT ** maybe we can't do it this way because the ID doesn't exist yet. Maybe this has to wait until
//     // the finalize sale step **************************

//     var sales_search = sales.map(x => {
//       if (x.sale_id == sale_id && x.state == 'created') return true
//       else return false
//     })

//     if (sales_search.filter(x => x == true).length != 1) throw ('Error finding exactly 1 active sale that matches the sale_id')

//     return { error: false, sale_data: sales_search[0] }
//   } catch (err) {
//     // console.log("Error: ", err)
//     return { error: true, err_msg: err }
//   }
// }

// async function escrow_placement () {
//   // Get equipment record

//   // verify serial number and no escrow

//   /*
//         insert record with updates

//         event: escrow_placement
//         escrow: true
//         timestamp: Date.now()

//     */

// }

export function TransferEscrowNetwork () {
  return async function (req, res, next) {
    try {
      const { sale_id: saleId } = req.body

      await axios({
        headers: { 'Content-Type': 'application/json' },
        method: 'post',
        auth: {
          username: process.env.MATERIALS_ID,
          password: process.env.MATERIALS_KEY,
        },
        url: process.env.SALES_API_URL + '/api/sales/read',
        saleId,
      })
    } catch (err) {
      next(err)
    }
  }
}

// this is a common hack it can be tied to process.env.NODE_ENV to avoid export when in production or development
export const __tests__ = process.env.NODE_ENV === 'test' ? { /* TransferEscrowController */ } : null
