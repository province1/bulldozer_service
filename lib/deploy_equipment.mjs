import { dbServiceRequest } from './db_service.mjs'
import verifySignature from './helpers.mjs'

async function deployEquip (messageJson, message, hash) {
  const address = verifySignature(message, hash).toLowerCase()

  let data = {
    collection: 'equipment',
    query: { serial: messageJson.serial_number, owner: address },
  }

  let serviceUrl = process.env.DB_SERVICE_URL + '/current_state'
  const equipmentSearch = await dbServiceRequest(data, serviceUrl)

  // ToDo: ** Add check so that if the bulldozer is already deployed in the intended location, there is no need to do anything.

  if (equipmentSearch.length === 0) {
    return { error: true, error_code: 0, error_msg: 'This equipment item does not have your address listed as owner.' }
  } else if (equipmentSearch.length === 1) {
    data = {
      collection: 'land_data',
      query: { owner: address, plot_int: parseInt(messageJson.new_location) },
      sort: { owner: 1 },
    }
    serviceUrl = process.env.DB_SERVICE_URL + '/query'
    const landCheck = await dbServiceRequest(data, serviceUrl)

    if (landCheck.length === 0) {
      return { error: true, error_code: 1, error_msg: "You don't have permission to clear this plot." }

      // ToDo: query should never return more than 1 object, but case needs to be covered just in case.
    } else {
      equipmentSearch[0].timestamp = Date.now() * 1000
      equipmentSearch[0].status = 'deployed'
      equipmentSearch[0].new_location = messageJson.new_location
      delete equipmentSearch[0]._id

      let data = {
        collection: 'equipment',
        entry_doc: equipmentSearch[0],
      }
      let serviceUrl = process.env.DB_SERVICE_URL + '/query'
      await dbServiceRequest(data, serviceUrl)

      // ToDo: User input for the query is very dangerous. Find something else.

      landCheck[0].template = landCheck[0].template.replace('Forest', 'Clearing')

      data = {
        collection: 'land_data',
        entry_doc: { timestamp: Date.now(), land_status: 'clearing', flash_over: true, img_url: 'https://eep.province.gg/images/bulldozer.png', template: landCheck[0].template },
      }

      serviceUrl = process.env.DB_SERVICE_URL + '/insert_single'
      const check = await dbServiceRequest(data, serviceUrl)

      if (check.acknowledged === true) {
        return { error: false }
      } else {
        return { error: true, error_code: 1, error_msg: "You don't have permission to clear this plot." }
      }
    }
  }
}

export { deployEquip }
