
import axios from 'axios'
import { database_service as databaseService } from 'data-service-adapter'

import { dbServiceRequest } from './db_service.mjs'

const outsideTime = new Date('Feb 5, 2022 14:30:00').getTime()
const provincialStartTime = new Date('Jan 6, 2031 14:30:00').getTime()

const dbService = databaseService(process.env.DB_SERVICE_URL, process.env.DB_SERVICE_ID, process.env.DB_SERVICE_KEY)

/**
* Receive the current Provincial Time as of right now in miliseconds.
*
*/

function currentProvincialTimeEpoch () {
  // This was a big mistake in the past becuase it was a global variable set at initial run time, but was never updated.
  const now = new Date().getTime()
  return provincialStartTime + (now - outsideTime) * 5
}

/**
* The total time needed to clear a 1km sq plot of land:
*
* 1000 milliseconds in 1 second. 60 Seconds in 1 minute. 60 minutes in 1 hour. 24 hours in 1 day. 365.25 /12 (days in an even month) * 3 months
*
*/
const clearingTimeRequired = 1000 * 60 * 60 * 24 * (365.25 / 12) * 3

/**
*   Provide the starting time in miliseconds that a clearing operation began. Receive back the percentage completion of the clearing operation.
*   @param {Int}
*/
function calcPctComplete (startTimeReal) {
  // convert start time into provincial time. This is the time that the action started in provincial time.
  const startTimeProvincial = provincialStartTime + (startTimeReal - outsideTime) * 5

  // Add the required clearing time to calculate the date the clearing will be completed in provincial time
  // var clearing_completion_time = start_time_provincial + clearing_time_required

  const pctComplete = Math.round((currentProvincialTimeEpoch() - startTimeProvincial) / clearingTimeRequired * 100)

  if (pctComplete > 100) {
    return 100
  } else if (pctComplete < 0) {
    return 0
  } else {
    return pctComplete
  }
}

/*
async function updateChanges (entryDoc, serviceUrl) {
  entryDoc.timestamp = Date.now()

  const data = {
    collection: 'equipment',
    entry_doc: entryDoc,
  }

  await dbServiceRequest(data, serviceUrl)

  return true
}
*/

export async function getPctComplete (serial) {
  const data = {
    collection: 'equipment',
    query: { serial },
  }

  // var service_url = process.env.DB_SERVICE_URL + "/current_state";
  // var result = await dbServiceRequest(data, service_url);
  const result = await dbService.data_op(data, 'current_state')

  if (result.error) {
    console.log({ error: true, error_msg: 'internal error', error_code: 500, result })
    return { error: true, error_msg: 'internal error', error_code: 500 }
  } else if (result.result.length === 0) {
    // Unless there has been fault, this likely means that the client side submited a serial number that is not in the clearing status. This could be a hacking attempt.
    console.log({ error: true, error_msg: 'internal error', error_code: 500, result, serial })
    return { error: true, error_msg: 'internal error', error_code: 500 }
  } else if (result.result[0].status !== 'deployed') {
    return { error: true, error_msg: 'Equipment not deployed.', error_code: 0 }
  }

  if (!result.error) {
    const percentComplete = calcPctComplete(result.result[0].timestamp)
    return { error: false, percent_complete: percentComplete }
  } else {
    // catch all
    return { error: true, error_msg: 'internal error', error_code: 500 }
  }
}

/**
 *
 *  Helper function to verify if the required amount of time for a clearing operation has passed or not. If so, change the state of the bulldozer and credit the user's account with 90 tons of timber.
 *  **Critial Function**. If this fails, users may not receive credit for clearing operations or may be exploited if it doesn't use the required amount of time.
 *  Functions added as input to make unit testing easier.
 *
 *  @param {Object} - Bulldozer state JSON object
 *  @param {Function} - Handler function when state change is required that will change the state of the bulldozer to idle.
 *  @param {Function} - Handler function when state change is required that will change the land to cleared.
 *  @param {Function} - Handler function when state change is required that will credit the user with 90 tons of timber.
 *
 * Returns a JSON with the result
 * @returns {JSON}
 * {boolean} process_complete: true if a clearing operation was complete
 * {boolean} err: true if the process had an error
 *
 */

async function completionCheck (state, updateHandler, landHandler, creditHandler) {
  if (state.status !== 'deployed') { return { process_complete: false, err: false, state } }

  // When it started, Provincial Time
  const startTimeProvincial = provincialStartTime + (state.timestamp - outsideTime) * 5

  // When it is completed, Provincial Time
  const completionTimeProvincial = startTimeProvincial + clearingTimeRequired

  if (currentProvincialTimeEpoch() > completionTimeProvincial) {
    state.status = 'idle'

    delete state._id

    const insertResult = await updateHandler(state)

    // ToDo: add error handling here in error == true

    // update land
    const landResult = await landHandler(state)
    // ToDo: add error handling here in error == true

    // credit owner
    const creditResult = await creditHandler(state.owner)
    // ToDo: add error handling here in error == true

    return { process_complete: true, update_handler_result: insertResult, land_handler_result: landResult, credit_handler_result: creditResult }
  }
  return { process_complete: false, err: false, state }
}

async function updateLand (doc) {
  /*
    * TODO: handle errors; don't let the app crash when they happen
    */
  // search for cleared land

  const data = {
    collection: 'land_data',
    query: { id: doc.new_location },
    sort: {},
  }

  const response = await dbService.data_op(data, 'current_state')

  // ToDo: Add Error handling in case length response == 0

  let land = response.result[0]

  land.land_status = 'cleared'
  land.timestamp = Date.now()

  delete land._id

  const data2 = {
    collection: 'land_data',
    entry_doc: land,
  }

  land = dbService.data_op(data2, 'state_change')

  return true
}

async function creditOwner (owner) {
  const serviceUrlCreate = process.env.MATERIAL_SERVICE_URL + '/create'
  const amount = 9000
  const type = 'timber'

  const data = {
    owner,
    amount,
    type,
  }

  try {
    const result = await axios({
      headers: { 'Content-Type': 'application/json' },
      method: 'post',
      auth: {
        username: process.env.MATERIALS_ID,
        password: process.env.MATERIALS_KEY,
      },
      url: serviceUrlCreate,
      data,
    })

    return result.data
  } catch (e) {
    return e
  }
}

/**
 *  Routine to check for equipment (bulldozers) that are deployed. If the bulldozers
 *  are complete, do nothing. If the bulldozers are done clearing a plot of land, change the
 *  status to idle and credit the user's account with timber.
 *
 *  Must be tested for exact milisecond precision.
 */

export async function updateStatus () {
  // ToDo: Replace this with data_op when current_state_many is added.
  const serviceUrl = process.env.DB_SERVICE_URL + '/current_state_many'

  const data = {

    collection: 'equipment',
    filter: {},
    grouper: 'serial',
  }

  const result = await dbServiceRequest(data, serviceUrl)

  if (result.error) {
    console.log({ error: true, error_msg: 'internal error', error_code: 500, result, service_url: serviceUrl, data: JSON.stringify(data) })
    return { error: true, error_msg: 'internal error', error_code: 500 }
  }

  const updateStatusHandler = async function (state) {
    const updateData = { collection: 'equipment', entry_doc: state }
    return await dbService.data_op(updateData, 'state_change')
  }

  return await Promise.all(result.map(async x => {
    return await completionCheck(x, updateStatusHandler, updateLand, creditOwner)
  }))
}

// this is a common hack it can be tied to process.env.NODE_ENV to avoid export when in production or development
export const __tests__ = process.env.NODE_ENV === 'test' ? { completionCheck, calcPctComplete } : null
