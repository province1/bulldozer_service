import * as chai from 'chai'

// theres no way around import/export
import { __tests__ } from '../lib/land_clearing.mjs'
chai.should()
const { completionCheck, calcPctComplete } = __tests__

// Test Date Calculations

const testDateStartNow = Date.now()
const testDate50Pct = testDateStartNow - (1000 * 60 * 60 * 24 * (365.25 / 12) * 3 * 0.5 / 5)
const testDateDone = testDateStartNow - 8640000000
const testDateRandomErr = testDateStartNow + 8640000000

describe('Calculate the percentage completion of land clearing in progress', function () {
  it('Should output 50% for a time = 45 days ago in provincial time', function () {
    // Miliseconds -> Seconds -> Minutes -> Hours -> -> days * # of days in a month * 3 months /2 (50% of total time required) -> provincial time required
    const testDate = Date.now() - (1000 * 60 * 60 * 24 * (365.25 / 12) * 3 * 0.5 / 5)
    calcPctComplete(testDate).should.equal(50)
  })

  it('Should output 0% for a clearing operation starting now', function () {
    calcPctComplete(Date.now()).should.equal(0)
  })

  it('Should output 100% for a clearing operation that started well over 90 provincial days ago', function () {
    const testDate = Date.now() - 8640000000
    calcPctComplete(testDate).should.equal(100)
  })

  it('Should output 0% for a clearing operation that has an erroneous date starting in the future.', function () {
    const testDate = Date.now() + 8640000000
    calcPctComplete(testDate).should.equal(0)
  })
})

describe('Test each clearing operation for completion, and if complete, make the updates to the bulldozer, land, and material services.', async function () {
  const test1 = { status: 'idle', it_msg: 'Should return false for bulldozers with status idle', process_complete: false }
  const test2 = { status: 'deployed', timestamp: testDateStartNow, it_msg: 'Should return false for clearing operations that just started', process_complete: false }
  const test3 = { status: 'deployed', timestamp: testDate50Pct, it_msg: 'Should return false for clearing operations that are 50% complete', process_complete: false }
  const test4 = { status: 'deployed', timestamp: testDateDone, it_msg: 'Should return true for clearing operations that are complete and return true for the update operations', process_complete: true }
  const test5 = { status: 'deployed', timestamp: testDateRandomErr, it_msg: 'Should return false for a clearing operation with an erroneous date starting in the future.', process_complete: false }

  const updateHandler = () => {
    return true
  }

  const testArray = [test1, test2, test3, test4, test5]

  await Promise.all(testArray.map(async x => {
    it(x.it_msg, async () => {
      const test = await completionCheck(x, updateHandler, updateHandler, updateHandler)
      test.process_complete.should.equal(x.process_complete)

      if (test.process_complete === true) {
        test.update_handler_result.should.equal(true)
        test.land_handler_result.should.equal(true)
        test.credit_handler_result.should.equal(true)
      }
    })
  }))
})
